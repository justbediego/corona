package com.diego.corona.persistance.model.preprocess;

import com.diego.corona.persistance.model.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "country_names")
public class CountryName extends BaseEntity {

    @Column
    private String name;

    @Column
    private String flagShortcut;

    @Column
    private String whatsInData;
}

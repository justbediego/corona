package com.diego.corona.persistance.model.preprocess;

import com.diego.corona.persistance.model.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "record_fields")
public class RecordField extends BaseEntity {

    @Column
    private String dataKey;

    @Column
    private String whatsInData;
}

package com.diego.corona.persistance.model.analyzation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnalyzedCountryDay {
    private Date day;
    private int confirmed;
    private double logConfirmed;
    private double logConfirmedSlope;
    private int death;
    private double logDeath;
    private double logDeathSlope;
    private double deathRate;
}

package com.diego.corona.persistance.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Subselect;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Subselect("select * from get_by_country")
public class DbCountryDay {
    @Id
    private UUID id;
    private Date day;
    private Integer confirmed;
    private Integer death;
}

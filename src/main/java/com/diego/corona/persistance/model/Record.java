package com.diego.corona.persistance.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "records")
public class Record extends BaseEntity {

    @ManyToOne
    private DayRecord dayRecord;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private String jsonData;
}

package com.diego.corona.persistance.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "day_records")
public class DayRecord extends BaseEntity {

    @Column
    private Date day;

    @OneToMany(targetEntity = Record.class, mappedBy = "dayRecord", fetch = FetchType.LAZY)
    private List<Record> records;
}

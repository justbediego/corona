package com.diego.corona.persistance.model.analyzation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnalyzedCountry {

    private UUID Id;

    private double growthRateSlope;

    private Integer currentConfirmed;

    private Integer currentDeath;

    private double currentMortalityRate;

    private double currentGrowthRate;

    private String countryName;

    private String flagShortcut;

    private List<AnalyzedCountryDay> countryDays = new ArrayList<>();

}

package com.diego.corona.persistance.repository;

import com.diego.corona.persistance.model.DayRecord;
import com.diego.corona.persistance.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RecordRepository extends JpaRepository<Record, UUID> {

    @Query("SELECT r FROM Record r WHERE r.dayRecord = ?1")
    List<Record> findAllByDay(DayRecord dayRecord);
}

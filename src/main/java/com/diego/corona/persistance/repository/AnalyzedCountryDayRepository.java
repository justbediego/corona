package com.diego.corona.persistance.repository;

import com.diego.corona.persistance.model.DbCountryDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface AnalyzedCountryDayRepository  extends JpaRepository<DbCountryDay, UUID> {

    @Query(value = "SELECT id, day, confirmed, death from get_by_country(:county_name)", nativeQuery = true)
    List<DbCountryDay> getAnalyzedData(@Param("county_name") String countyName);
}

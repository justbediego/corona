package com.diego.corona.persistance.repository;

import com.diego.corona.persistance.model.DayRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.UUID;

@Repository
public interface DayRecordRepository extends JpaRepository<DayRecord, UUID> {
    @Query("SELECT dr FROM DayRecord dr WHERE dr.day = ?1")
    DayRecord findByDate(Date date);
}

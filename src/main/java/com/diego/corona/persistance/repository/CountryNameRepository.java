package com.diego.corona.persistance.repository;

import com.diego.corona.persistance.model.preprocess.CountryName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CountryNameRepository extends JpaRepository<CountryName, UUID> {

}
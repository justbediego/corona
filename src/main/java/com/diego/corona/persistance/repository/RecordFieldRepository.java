package com.diego.corona.persistance.repository;

import com.diego.corona.persistance.model.preprocess.RecordField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RecordFieldRepository extends JpaRepository<RecordField, UUID> {

    @Query("SELECT rf FROM RecordField rf WHERE rf.dataKey = ?1")
    List<RecordField> findAllByKey(String dataKey);
}

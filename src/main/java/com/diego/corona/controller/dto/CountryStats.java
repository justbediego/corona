package com.diego.corona.controller.dto;

import com.diego.corona.persistance.model.analyzation.AnalyzedCountry;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class CountryStats {

    private UUID id;

    private double growthRate;

    private double growthRateSlope;

    private Integer confirmed;

    private Integer death;

    private double mortalityRate;

    private String countryName;

    private String flagShortcut;

    public static CountryStats fromAnalyzedCountry(AnalyzedCountry analyzedCountry) {
        return CountryStats.builder()
                .id(analyzedCountry.getId())
                .confirmed(analyzedCountry.getCurrentConfirmed())
                .countryName(analyzedCountry.getCountryName())
                .death(analyzedCountry.getCurrentDeath())
                .flagShortcut(analyzedCountry.getFlagShortcut())
                .growthRate(analyzedCountry.getCurrentGrowthRate())
                .growthRateSlope(analyzedCountry.getGrowthRateSlope())
                .mortalityRate(analyzedCountry.getCurrentMortalityRate())
                .build();
    }
}

package com.diego.corona.controller.dto;

public enum CountryDataType {
    RAW,
    LOGARITHMIC,
    GROWTH_RATE,
    MORTALITY_RATE
}

package com.diego.corona.controller.dto;

public enum SortType {
    GROWTH_RATE_SLOPE,
    GROWTH_RATE,
    MORTALITY_RATE,
    CONFIRMED,
    DEATH,
}

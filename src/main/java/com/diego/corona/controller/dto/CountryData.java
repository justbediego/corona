package com.diego.corona.controller.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CountryData {
    public CountryData(Integer seriesCount) {
        for (int i = 0; i < seriesCount; i++) {
            dataSeries.add(new ArrayList<>());
        }
    }

    private List<List<Object>> dataSeries = new ArrayList<>();
}

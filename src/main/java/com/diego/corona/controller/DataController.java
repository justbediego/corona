package com.diego.corona.controller;

import com.diego.corona.controller.dto.CountryData;
import com.diego.corona.controller.dto.CountryDataType;
import com.diego.corona.controller.dto.CountryStats;
import com.diego.corona.controller.dto.SortType;
import com.diego.corona.persistance.model.analyzation.AnalyzedCountry;
import com.diego.corona.persistance.model.analyzation.AnalyzedCountryDay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.diego.corona.controller.dto.CountryDataType.MORTALITY_RATE;

@RestController
@RequestMapping("/data")
public class DataController {

    @Autowired
    private List<AnalyzedCountry> getAnalyzedCountries;

    @GetMapping("/getCountryStats/{sortType}/{sortAscending}")
    public List<CountryStats> getCountryStats(@PathVariable SortType sortType, @PathVariable boolean sortAscending, @RequestParam(required = false) String phrase) {
        Comparator<AnalyzedCountry> sorter = null;
        switch (sortType) {
            case MORTALITY_RATE:
                sorter = Comparator.comparing(AnalyzedCountry::getCurrentMortalityRate);
                break;
            case CONFIRMED:
                sorter = Comparator.comparing(AnalyzedCountry::getCurrentConfirmed);
                break;
            case DEATH:
                sorter = Comparator.comparing(AnalyzedCountry::getCurrentDeath);
                break;
            case GROWTH_RATE:
                sorter = Comparator.comparing(AnalyzedCountry::getCurrentGrowthRate);
                break;
            //case GROWTH_RATE_SLOPE:
            default:
                sorter = Comparator.comparing(AnalyzedCountry::getGrowthRateSlope);
                break;
        }
        if (!sortAscending) {
            sorter = sorter.reversed();
        }
        return getAnalyzedCountries.stream()
                .filter(analyzedCountry -> (phrase == null || phrase.isEmpty()) ? true : analyzedCountry.getCountryName().toLowerCase().contains(phrase.toLowerCase()))
                .sorted(sorter)
                .map(CountryStats::fromAnalyzedCountry)
                .collect(Collectors.toList());
    }

    @GetMapping("/getCountryData/{countryId}/{dataType}")
    public CountryData getCountryData(@PathVariable UUID countryId, @PathVariable CountryDataType dataType) {
        return getAnalyzedCountries.stream()
                .filter(analyzedCountry -> analyzedCountry.getId().equals(countryId))
                .map(analyzedCountry -> {
                    CountryData countryData = new CountryData(dataType == MORTALITY_RATE ? 2 : 3);
                    analyzedCountry
                            .getCountryDays()
                            .stream()
                            .sorted(Comparator.comparing(AnalyzedCountryDay::getDay))
                            .forEach(analyzedCountryDay -> {
                                countryData.getDataSeries().get(0).add(analyzedCountryDay.getDay());
                                switch (dataType) {
                                    case RAW:
                                        countryData.getDataSeries().get(1).add(analyzedCountryDay.getConfirmed());
                                        countryData.getDataSeries().get(2).add(analyzedCountryDay.getDeath());
                                        break;
                                    case LOGARITHMIC:
                                        countryData.getDataSeries().get(1).add(analyzedCountryDay.getLogConfirmed());
                                        countryData.getDataSeries().get(2).add(analyzedCountryDay.getLogDeath());
                                        break;
                                    case GROWTH_RATE:
                                        countryData.getDataSeries().get(1).add(analyzedCountryDay.getLogConfirmedSlope());
                                        countryData.getDataSeries().get(2).add(analyzedCountryDay.getLogDeathSlope());
                                        break;
                                    case MORTALITY_RATE:
                                        countryData.getDataSeries().get(1).add(analyzedCountryDay.getDeathRate());
                                        break;
                                }
                            });
                    return countryData;
                }).findFirst().orElseThrow();
    }
}

package com.diego.corona.engine;

import com.diego.corona.persistance.model.DbCountryDay;
import com.diego.corona.persistance.model.analyzation.AnalyzedCountry;
import com.diego.corona.persistance.model.analyzation.AnalyzedCountryDay;
import com.diego.corona.persistance.model.preprocess.CountryName;
import com.diego.corona.persistance.repository.AnalyzedCountryDayRepository;
import com.diego.corona.persistance.repository.CountryNameRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AnalyzerEngine {

    @Autowired
    private CountryNameRepository countryNameRepository;

    @Autowired
    private AnalyzedCountryDayRepository analyzedCountryDayRepository;

    private List<AnalyzedCountry> analyzedCountries = new ArrayList<>();

    private List<AnalyzedCountry> analyzedCountriesTemp = new ArrayList<>();

    @Bean
    public List<AnalyzedCountry> getAnalyzedCountries() {
        return analyzedCountries;
    }

    private double getTimeBasedOnDay(Date date) {
        return Double.valueOf(date.getTime()) / (24 * 3600 * 1000);
    }

    public void reAnalyze() {
        @Data
        @AllArgsConstructor
        class CountryShape {
            private UUID id;
            private String name;
            private String flagShortcut;
        }
        analyzedCountriesTemp.clear();
        List<CountryName> allCountries = countryNameRepository.findAll();
        List<CountryShape> countriesInShape = allCountries.stream()
                .map(country -> country.getName())
                .distinct()
                .map(countryName -> allCountries.stream()
                        .filter(country -> country.getName().equals(countryName))
                        .sorted(Comparator.comparing(CountryName::getFlagShortcut, Comparator.nullsLast(Comparator.naturalOrder())))
                        .map(country -> new CountryShape(country.getId(), country.getName(), country.getFlagShortcut()))
                        .findFirst()
                        .get())
                .collect(Collectors.toList());
        int countryCount = 0;
        for (CountryShape country : countriesInShape) {
            log.info(String.format("Analyzing %d/%d, %s", countryCount++, countriesInShape.size(), country.getName()));
            AnalyzedCountry analyzedCountry = AnalyzedCountry.builder()
                    .Id(country.getId())
                    .countryName(country.getName())
                    .flagShortcut(country.getFlagShortcut())
                    .countryDays(new ArrayList<>())
                    .build();
            List<DbCountryDay> countryDayData = analyzedCountryDayRepository.getAnalyzedData(analyzedCountry.getCountryName());
            SimpleRegression confirmedRegression = new SimpleRegression();
            SimpleRegression deathRegression = new SimpleRegression();
            countryDayData.sort(Comparator.comparing(DbCountryDay::getDay));
            for (int i = 0; i < countryDayData.size(); i++) {
                Date day = countryDayData.get(i).getDay();
                if (i == 0) {
                    //before first day
                    AnalyzedCountryDay beforeFirstDay = AnalyzedCountryDay.builder()
                            .day(new Date(day.getTime() - (24 * 3600 * 1000)))
                            .confirmed(0)
                            .logConfirmed(0)
                            .logConfirmedSlope(0)
                            .death(0)
                            .logDeath(0)
                            .logDeathSlope(0)
                            .build();
                    analyzedCountry.getCountryDays().add(beforeFirstDay);
                    confirmedRegression.addData(getTimeBasedOnDay(beforeFirstDay.getDay()), 0);
                    deathRegression.addData(getTimeBasedOnDay(beforeFirstDay.getDay()), 0);
                }
                AnalyzedCountryDay yesterdayData = analyzedCountry.getCountryDays().get(i);
                Integer confirmed = countryDayData.get(i).getConfirmed();
                Integer death = countryDayData.get(i).getDeath();
                //calculations
                if (confirmed == 0) {
                    confirmed = yesterdayData.getConfirmed();
                }
                if (death == 0) {
                    death = yesterdayData.getDeath();
                }
                //log
                double logConfirmed = Math.log10(confirmed + 1) / Math.log10(2);
                double logDeath = Math.log10(death + 1) / Math.log10(2);
                //regression
                confirmedRegression.addData(getTimeBasedOnDay(day), logConfirmed);
                deathRegression.addData(getTimeBasedOnDay(day), logDeath);
                //end
                AnalyzedCountryDay analyzedCountryDay = AnalyzedCountryDay.builder()
                        .day(day)
                        .confirmed(confirmed)
                        .logConfirmed(logConfirmed)
                        .logConfirmedSlope(confirmedRegression.getSlope())
                        .death(death)
                        .logDeath(logDeath)
                        .logDeathSlope(deathRegression.getSlope())
                        .deathRate(confirmed > 0 ? Double.valueOf(death) / confirmed : 0)
                        .build();
                analyzedCountry.getCountryDays().add(analyzedCountryDay);
            }
            if (analyzedCountry.getCountryDays().size() >= 14) {
                AnalyzedCountryDay todayData = analyzedCountry.getCountryDays().get(analyzedCountry.getCountryDays().size() - 1);
                AnalyzedCountryDay yesterdayData = analyzedCountry.getCountryDays().get(analyzedCountry.getCountryDays().size() - 2);
                analyzedCountry.setGrowthRateSlope(todayData.getLogConfirmedSlope() - yesterdayData.getLogConfirmedSlope());
                analyzedCountry.setCurrentConfirmed(todayData.getConfirmed());
                analyzedCountry.setCurrentDeath(todayData.getDeath());
                analyzedCountry.setCurrentGrowthRate(todayData.getLogConfirmedSlope());
                analyzedCountry.setCurrentMortalityRate(todayData.getDeathRate());
                analyzedCountriesTemp.add(analyzedCountry);
            } else {
                log.info("Not enough data for " + analyzedCountry.getCountryName());
            }
        }
        analyzedCountries.clear();
        analyzedCountries.addAll(analyzedCountriesTemp);
    }
}

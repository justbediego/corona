package com.diego.corona.engine;

import com.diego.corona.persistance.model.DayRecord;
import com.diego.corona.persistance.model.Record;
import com.diego.corona.persistance.repository.DayRecordRepository;
import com.diego.corona.persistance.repository.RecordRepository;
import com.google.gson.Gson;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Component
public class DataEngine {

    @Value("${spring.application.git-folder}")
    private String gitFolder;

    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private DayRecordRepository dayRecordRepository;

    @Autowired
    private AnalyzerEngine analyzerEngine;

    @Scheduled(fixedDelay = 3600000)
    public void doEngineThings() {
        try {
            gitPull();
            HashMap<Date, String> dataFiles = getDataFiles();
            processDataFiles(dataFiles);
            dayRecordRepository.flush();
            analyzerEngine.reAnalyze();
        } catch (Exception e) {
            log.error("Data gathering failed due to '{}'", e.getMessage());
        }
    }

    private HashMap<Date, String> getDataFiles() throws ParseException {
        HashMap<Date, String> result = new HashMap<>();
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        File folder = new File(String.format("%s/csse_covid_19_data/csse_covid_19_daily_reports/", gitFolder));
        for (File file : folder.listFiles()) {
            if (file.isFile() && file.getName().matches("^\\d{2}(-)\\d{2}(-)\\d{4}(.csv)$")) {
                Date date = format.parse(file.getName().substring(0, 10));
                result.put(date, file.getAbsolutePath());
            }
        }
        return result;
    }

    private void processDataFiles(HashMap<Date, String> data) throws IOException {
        Gson gson = new Gson();
        for (Map.Entry<Date, String> pair : data.entrySet()) {
            DayRecord dayRecord = dayRecordRepository.findByDate(pair.getKey());
            if (dayRecord == null) {
                dayRecord = dayRecordRepository.save(DayRecord.builder()
                        .day(pair.getKey())
                        .records(new ArrayList<>())
                        .build());
                CSVReader csvReader = new CSVReader(new FileReader(pair.getValue()));
                boolean isHeader = true;
                List<String> headers = new ArrayList<>();
                String[] values;
                while ((values = csvReader.readNext()) != null) {
                    if (isHeader) {
                        for (String key : values) {
                            headers.add(key);
                        }
                        isHeader = false;
                    } else {
                        Map<String, String> keyValueData = new HashMap<>();
                        for (int i = 0; i < values.length; i++) {
                            keyValueData.put(headers.get(i), values[i]);
                        }
                        recordRepository.save(Record.builder()
                                .dayRecord(dayRecord)
                                .jsonData(gson.toJson(keyValueData))
                                .build());
                    }
                }
            }
        }
    }

    private void gitPull() throws IOException, InterruptedException {
        log.info("doing a 'git pull'");
        runCommand(Paths.get(gitFolder), "git", "pull");
        log.info("git pulled");
    }

    private void runCommand(Path directory, String... command) throws IOException, InterruptedException {
        if (!Files.exists(directory)) {
            throw new RuntimeException("can't run command in non-existing directory '" + directory + "'");
        }
        ProcessBuilder pb = new ProcessBuilder()
                .command(command)
                .directory(directory.toFile());
        Process p = pb.start();
        int exit = p.waitFor();
        if (exit != 0) {
            throw new RuntimeException(String.format("runCommand returned %d", exit));
        }
    }
}

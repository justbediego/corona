import React from 'react';
import Dashboard from "./dashboard/Dashboard";
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from 'react-router-dom';
import Login from "./dashboard/Login";

export default function App() {
    const theme = React.useMemo(
        () =>
            createMuiTheme({
                palette: {
                    primary: {
                        main: '#004d40',
                    },
                    secondary: {
                        main: '#ffee58',
                    },
                },
                typography: {
                    fontFamily: [
                        '-apple-system',
                        'BlinkMacSystemFont',
                        '"Segoe UI"',
                        'Roboto',
                        '"Helvetica Neue"',
                        'Arial',
                        'sans-serif',
                        '"Apple Color Emoji"',
                        '"Segoe UI Emoji"',
                        '"Segoe UI Symbol"',
                    ].join(','),
                }
            })
    , []);

    return (
        <ThemeProvider theme={theme}>
            <Router>
                <Switch>
                    <Route path="/corona" component={Dashboard}/>
                    <Route component={Login}/>
                </Switch>
            </Router>
        </ThemeProvider>
    );
}

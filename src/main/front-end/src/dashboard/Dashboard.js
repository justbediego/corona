import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Copyright from "./copyright/Copyright";
import CountryList from "./countryList/CountryList";
import WorldData from "./worldData/WorldData";
import Grid from "@material-ui/core/Grid";
import InsertChartRoundedIcon from '@material-ui/icons/InsertChartRounded';
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 40, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100%)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        //height: '100vh',
        //overflow: 'hidden',
    },
    countryContainer: {
        padding: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
    hintText: {
        padding: theme.spacing(3),
        marginBottom: theme.spacing(3)
    }
}));

export default function Dashboard() {
    const classes = useStyles();
    const [activePage, setActivePage] = useState(0);

    const Advertisement = () => {
        return (
            <Paper className={classes.paper}>item</Paper>
        )
    }

    const getHintDescription = () => {
        return (
            <>
                <Paper elevation={3} className={classes.hintText}>
                    <Typography variant={'subtitle1'}>
                        "The greatest shortcoming of the human race is our inability to understand the exponential function".  -Albert Allen Bartlett
                    </Typography>
                        <Typography variant={'h6'}>
                            About these results
                    </Typography>
                        <Typography variant={'subtitle1'} align={'justify'}>
                            What you see is the result of our analysis on the data provided by worldmeters website regarding COVID-19 confirmed and death cases. By applying a day to day linear regression over the logarithmic scale of confirmed and death cases, we extracted a coefficient we call <b>"Growth Rate"</b>. In a non-disturbed system, the "growth rate" should stay the same over time and we can analyze the daily disturbance of the system by comparing its "growth rate" to the days before.
                    </Typography>
                </Paper>
                <Tabs
                    value={activePage}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={(event, newTabIndex) => setActivePage(newTabIndex)}>
                    <Tab label="World" />
                    <Tab label="By Country" />
                </Tabs>
            </>
        );
    }

    const getPageContent = () => (
        <Grid container className={classes.countryContainer}>
            <Grid item xs={12}>
                {getHintDescription()}
            </Grid>
            {activePage === 1 && <CountryList />}
            {activePage === 0 && <WorldData />}
        </Grid>
    );


    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="absolute" className={clsx(classes.appBar)}>
                <Toolbar className={classes.toolbar}>
                    <InsertChartRoundedIcon fontSize="large" style={{ paddingRight: 5 }} />
                    <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                        Celecit COVID-19
                    </Typography>
                </Toolbar>
            </AppBar>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg">
                    {getPageContent()}
                    <Box pt={4}>
                        <Copyright />
                    </Box>
                </Container>
            </main>
        </div>
    );
}


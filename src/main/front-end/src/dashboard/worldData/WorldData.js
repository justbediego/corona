import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {HorizontalBar} from 'react-chartjs-2'
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    worldGrid: {
        marginTop: 20,
        padding: 15
    },
    paper: {
        padding: 15
    },
    scrollable: {
        overflowX: 'scroll',
        width: '100%'
    }

}));

export default function WorldData() {
    const classes = useStyles();
    const [allCountriesScore, setAllCountriesScore] = useState([]);
    const [labels, setLabels] = useState([]);
    const [colors, setColors] = useState([]);
    const [countryScores, setCountryScores] = useState([]);

    const getAllCountriesScore = () => {
        fetch('/data/getAllCountriesScore')
            .then(res => res.json())
            .then(res => {
                setLabels(res.map((country, index) => (index + 1) + "- " + country.countryName.slice(0, 13)));
                setCountryScores(res.map(country => country.confirmedSlopeIncrease));
                setAllCountriesScore(res);
                setColors(res.map(country => getCountryColor(country.confirmedSlopeIncrease)));
            });

    }

    function getCountryColor(slope) {
        let value = slope * 75 + .5;
        if (value < 0) {
            value = 0;
        }
        if (value > 1) {
            value = 1;
        }
        //value from 0 to 1
        var hue = ((1 - value) * 120).toString(10);
        return ["hsl(", hue, ",100%,50%)"].join("");
    }

    useEffect(() => {
        getAllCountriesScore();
    }, []);

    const hasNoData = (allCountriesScore.length === 0);
    if (hasNoData) {
        return (
            <Typography align='center' color='error' variant='h6' style={{marginTop: 70}}>
                Sorry for the inconvenience, the server is busy analyzing data. Please try again in a few minutes.
            </Typography>
        );
    }
    return (
        <>
            <Grid item xs={12} className={classes.worldGrid}>
                <Paper elevation={3} className={classes.paper}>
                    <HorizontalBar
                        height={1000}
                        data={{labels: labels, datasets: [{data: countryScores, backgroundColor: colors}]}}
                        options={{
                            legend: {display: false},
                            title: {
                                display: true,
                                text: "Global Growth Rate",
                                horizontalAlign: "right",
                                verticalAlign: "left",
                            },
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItem, data) {
                                        const hint = tooltipItem.value > 0 ? "(Speeding up)" : "(Slowing down)";
                                        return Number(tooltipItem.value).toFixed(5) + " " + hint;
                                    }
                                }
                            }
                        }}
                    />
                </Paper>

            </Grid>
        </>
    );
}

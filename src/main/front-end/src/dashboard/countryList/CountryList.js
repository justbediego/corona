import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InfiniteScroll from "react-infinite-scroll-component";
import Grid from "@material-ui/core/Grid";
import AnalyzedCountry from "../analyzedcountry/AnalyzedCountry";
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    loading: {
        marginLeft: 110,
        fontSize: 17,
        color: 'gray',
        fontWeight: 'none',
        fontFamily: 'monospace'
    },
}));

export default function CountryList() {
    const classes = useStyles();
    const [topCountries, setTopCountries] = useState([]);
    const [currentDataIndex, setCurrentDataIndex] = useState(0);
    const [hasMoreData, setHasMoreData] = useState(true);
    const getNextTopCountries = () => {
        fetch('/data/getTopCountries/' + currentDataIndex)
            .then(res => res.json())
            .then(res => {
                if (res.length === 0) {
                    setHasMoreData(false);
                } else {
                    setTopCountries(prevState => prevState.concat(res));
                    setCurrentDataIndex(prevState => prevState + 1);
                }
            });
    }

    useEffect(() => {
        getNextTopCountries();
    }, []);


    const hasNoData = (topCountries.length === 0) && (hasMoreData === false);
    if (hasNoData) {
        return (
            <Typography align='center' color='error' variant='h6' style={{ marginTop: 70 }}>
                Sorry for the inconvenience, the server is busy analyzing data. Please try again in a few minutes.
            </Typography>
        );
    }
    return (
        <InfiniteScroll
            dataLength={topCountries.length} //This is important field to render the next data
            next={getNextTopCountries}
            hasMore={hasMoreData}
            //height='100vh'
            startY={true}
            loader={<h4 className={classes.loading}>Loading countries...</h4>}>
            <div style={{ margin: 24 }}>

                <Grid container spacing={2}>
                    {topCountries.map((country, index) =>
                        <Grid item lg={6} xs={12} key={index}>
                            <AnalyzedCountry country={country} rank={index + 1} />
                        </Grid>
                    )}
                </Grid>
            </div>
        </InfiniteScroll>
    );
}

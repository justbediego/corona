import React, {useEffect} from "react";
import {makeStyles} from '@material-ui/core/styles';
import {Line} from 'react-chartjs-2';
import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";
import CardHeader from "@material-ui/core/CardHeader";
import Card from "@material-ui/core/Card";
import ReactCountryFlag from "react-country-flag";
import Tabs from "@material-ui/core/Tabs";
import CardContent from "@material-ui/core/CardContent";
import Tab from "@material-ui/core/Tab";
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

const useStyles = makeStyles((theme) => ({
    cardHeader: {
        backgroundColor: '#dfecec',
    },
    tabPanel: {
        paddingTop: 22,
        paddingLeft: 9,
        paddingRight: 9,
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`
    },
    graphTab: {
        minWidth: '135px'
    },
    appBar: {
        backgroundColor: '#c7c7bf',
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    subHeader: {
        fontWeight: 'bold'
    },
    true: {
        color: 'green',
        fontSize: 18,
        fontWeight: 'bold',
        paddingLeft: 13
    },
    false: {
        color: 'red',
        fontSize: 18,
        fontWeight: 'bold',
        paddingLeft: 13
    },
    height: {
        height: '100vh'
    },
    width: {
        width: '100vh'
    },
    downArrow: {
        marginBottom: "-10px"
    }
}));


function DayBaseLineChart(props) {
    const {daySeries, datasets, options, ...other} = props;
    for (let dataset in datasets) {
        const data = datasets[dataset].data;
        const result = [];
        for (let i = 0; i < daySeries.length; i++) {
            result.push({x: daySeries[i], y: data[i]});
        }
        datasets[dataset].data = result;
    }
    return (
        <Line {...other}
              options={
                  {
                      ...options,
                      padding: "0px",
                      defaultFontSize: "10px",
                      responsive: true,
                      maintainAspectRatio: false,
                      scales: {
                          xAxes: [{
                              type: 'time',
                              time: {
                                  tooltipFormat: 'MMM DD YYYY',
                                  displayFormats: {
                                      'millisecond': 'MMM DD',
                                      'second': 'MMM DD',
                                      'minute': 'MMM DD',
                                      'hour': 'MMM DD',
                                      'day': 'MMM DD',
                                      'week': 'MMM DD',
                                      'month': 'MMM DD',
                                      'quarter': 'MMM DD',
                                      'year': 'MMM DD',
                                  }
                              }
                          }]
                      },
                      layout: {
                          padding: {
                              left: 0,
                              right: 0,
                              top: 0,
                              bottom: 0
                          }
                      },
                      legend: {
                          display: true,
                          position: 'top',
                          labels: {
                              boxWidth: 8,
                              fontSize: 9,
                              padding: 10,
                              lineCap: 0,
                          }
                      }
                  }}
              data={{datasets}}></Line>);
}


export default function AnalyzedCountry(props) {
    const {country, rank} = props;

    const classes = useStyles();
    const [selectedTab, setSelectedTab] = React.useState(2);
    const [dayLabels, setDayLabels] = React.useState([]);
    const [confirmed, setConfirmed] = React.useState([]);
    const [death, setDeath] = React.useState([]);
    const [logConfirmed, setLogConfirmed] = React.useState([]);
    const [logConfirmedSlope, setLogConfirmedSlope] = React.useState([]);
    const [logDeath, setLogDeath] = React.useState([]);
    const [logDeathSlope, setLogDeathSlope] = React.useState([]);
    const [deathRate, setDeathRate] = React.useState([]);

    const handleTabChange = (event, newTabIndex) => {
        setSelectedTab(newTabIndex);
    };

    function TabPanel(props) {
        const {children, selectedTab, index, ...other} = props;
        return (
            <Grid hidden={selectedTab !== index}
                  className={classes.tabPanel}
                  {...other}>
                {selectedTab === index && children}
            </Grid>
        );
    }

    useEffect(() => {
        setDayLabels(country.countryDays.slice(5).map(countryDay => new Date(countryDay.day.slice(0, 10))));
        setConfirmed(country.countryDays.slice(5).map(countryDay => countryDay.confirmed));
        setDeath(country.countryDays.slice(5).map(countryDay => countryDay.death));
        setLogConfirmed(country.countryDays.slice(5).map(countryDay => Number(countryDay.logConfirmed).toFixed(5)));
        setLogConfirmedSlope(country.countryDays.slice(5).map(countryDay => Number(countryDay.logConfirmedSlope).toFixed(5)));
        setLogDeath(country.countryDays.slice(5).map(countryDay => Number(countryDay.logDeath).toFixed(5)));
        setLogDeathSlope(country.countryDays.slice(5).map(countryDay => Number(countryDay.logDeathSlope).toFixed(5)));
        setDeathRate(country.countryDays.slice(5).map(countryDay => Number(countryDay.deathRate * 100).toFixed(5)));
    }, []);

    return (
        <Card elevation={8}>
            <CardHeader
                avatar=
                    {
                        country.flagShortcut &&
                        <ReactCountryFlag countryCode={country.flagShortcut}
                                          style={{fontSize: 43, border: 1, borderRadius: 12}} svg/>
                    }
                className={classes.cardHeader}
                title={rank + "- " + country.countryName}
                subheader={
                    <label>
                        Todays vs Yesterday:
                        <label className={(country.confirmedSlopeIncrease < 0) ? classes.true : classes.false}>
                            {Number(country.confirmedSlopeIncrease).toFixed(5)}
                            {country.confirmedSlopeIncrease >= 0 && <><ArrowUpwardIcon/> Speeding up</>}
                            {country.confirmedSlopeIncrease < 0 && <><ArrowDownwardIcon
                                className={classes.downArrow}/> Slowing down</>}
                        </label>

                    </label>
                }
                classes={{
                    title: classes.title,
                    subheader: classes.subHeader,
                }}
            />
            <CardContent style={{padding: 8}}>
                <Grid container>
                    <Grid item xs={3}>
                        <Hidden only={['md', 'sm', 'xs']}>
                            <Tabs
                                orientation="vertical"
                                variant="scrollable"
                                value={selectedTab}
                                onChange={handleTabChange}
                                className={classes.tabs}>
                                <Tab label="Raw Data" aria-controls="tab_0" id="tab_0" className={classes.graphTab}/>
                                <Tab label="Logarithmic" aria-controls="tab_1" id="tab_1" className={classes.graphTab}/>
                                <Tab label="Growth Rate" aria-controls="tab_2" id="tab_2" className={classes.graphTab}/>
                                <Tab label="Mortality Rate" aria-controls="tab_3" id="tab_3"
                                     className={classes.graphTab}/>
                            </Tabs>
                        </Hidden>
                        <Hidden only={['xl', 'lg']}>
                            <Tabs
                                orientation="vertical"
                                variant="scrollable"
                                value={selectedTab}
                                onChange={handleTabChange}
                                className={classes.tabs}>
                                <Tab label="Raw" aria-controls="tab_0" id="tab_4" className={classes.graphTab}/>
                                <Tab label="Log" aria-controls="tab_1" id="tab_5" className={classes.graphTab}/>
                                <Tab label="Growth" aria-controls="tab_2" id="tab_6" className={classes.graphTab}/>
                                <Tab label="Mortality" aria-controls="tab_3" id="tab_7" className={classes.graphTab}/>
                            </Tabs>
                        </Hidden>
                    </Grid>
                    <Grid item xs={9}>
                        <TabPanel selectedTab={selectedTab} index={0}>
                            <DayBaseLineChart daySeries={dayLabels}
                                              datasets={[
                                                  {
                                                      label: 'Confirmed Cases',
                                                      backgroundColor: 'rgba(75,192,192,0.4)',
                                                      borderColor: 'rgba(75,192,192,1)',
                                                      data: confirmed
                                                  },
                                                  {
                                                      label: 'Death Cases',
                                                      backgroundColor: 'rgba(230, 147, 147, 0.5)',
                                                      borderColor: 'rgba(230, 147, 147, 1)',
                                                      data: death
                                                  }
                                              ]}
                            />
                        </TabPanel>
                        <TabPanel selectedTab={selectedTab} index={1}>
                            <DayBaseLineChart daySeries={dayLabels}
                                              datasets={[
                                                  {
                                                      label: 'Confirmed Cases',
                                                      backgroundColor: 'rgba(202, 230, 193, 0.4)',
                                                      borderColor: 'rgba(202, 230, 193, 1)',
                                                      data: logConfirmed,
                                                  },
                                                  {
                                                      label: 'Death Cases',
                                                      backgroundColor: 'rgba(173, 190, 222, 0.6)',
                                                      borderColor: 'rgba(173, 190, 222, 1)',
                                                      data: logDeath
                                                  }
                                              ]}
                            />
                        </TabPanel>
                        <TabPanel selectedTab={selectedTab} index={2}>
                            <DayBaseLineChart daySeries={dayLabels}
                                              datasets={[
                                                  {
                                                      label: 'Confirmed Growth',
                                                      backgroundColor: 'rgba(170, 207, 207, 0.5)',
                                                      borderColor: 'rgba(170, 207, 207, 1)',
                                                      data: logConfirmedSlope,
                                                  },
                                                  {
                                                      label: 'Death Growth',
                                                      backgroundColor: 'rgba(247, 156, 183, 0.6)',
                                                      borderColor: 'rgba(247, 156, 183)',
                                                      data: logDeathSlope
                                                  }
                                              ]}
                            />
                        </TabPanel>
                        <TabPanel selectedTab={selectedTab} index={3}>
                            <DayBaseLineChart daySeries={dayLabels}
                                              datasets={[
                                                  {
                                                      label: 'Mortality Rate',
                                                      backgroundColor: 'rgba(164, 197, 198, 0.4)',
                                                      borderColor: 'rgba(164, 197, 198, 1)',
                                                      data: deathRate,
                                                  }
                                              ]}
                            />
                        </TabPanel>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
}



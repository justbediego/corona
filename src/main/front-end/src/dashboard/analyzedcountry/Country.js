import React from 'react';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';


export default function App() {
    const theme = React.useMemo(
        () =>
            createMuiTheme({
                palette: {
                    primary: {
                        main: '#004d40',
                    },
                    secondary: {
                        main: '#ffee58',
                    },
                },
                typography: {
                    fontFamily: [
                        '-apple-system',
                        'BlinkMacSystemFont',
                        '"Segoe UI"',
                        'Roboto',
                        '"Helvetica Neue"',
                        'Arial',
                        'sans-serif',
                        '"Apple Color Emoji"',
                        '"Segoe UI Emoji"',
                        '"Segoe UI Symbol"',
                    ].join(','),
                }
            })
    );

    return (
       <label>country</label>
    );
}
